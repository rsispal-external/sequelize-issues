import { map } from 'lodash';
import { pathOr } from 'ramda';

/* Database Model */
import LocalDBModel from '../../database/models/model.js';

export default {

  async testFunction1({ references }) {
      const LocalDB = await LocalDBModel.get();
      return LocalDB.Panels
            .findAll({
                where: { reference: references },
                include: [
                    {
                        model: LocalDB.PanelEvents,
                        order: [['timestamp', 'DESC']],
                        limit: 3
                    },
                    {
                        model: LocalDB.Communicators,
                        include: [
                            {
                                model: LocalDB.CommunicatorStatusUpdates,
                                order: [['timestamp', 'DESC']],
                                limit: 1
                            }
                        ]
                    }
                ],
                returning: true,
                logging: false
            })
            .then(response => {
                return response;
                // console.log(response);
                // return response;
                // map(response, panel => {
                //     const panelEvents = pathOr([], ['PanelEvents'], panel);
                //     const communicators = pathOr([], ['Communicators'], panel);
                //     const communicatorStatusUpdates = pathOr([], ['Communicators', 'CommunicatorStatusUpdates'], panel);
                //     console.log(`Panel - ${panel.reference}`);
                //     console.log(`- PanelEvents - ${panelEvents.length}`);
                //     console.log(`- Communicators - ${communicators.length}`);
                //     console.log(`- - CommunicatorStatusUpdates - ${communicatorStatusUpdates.length}`);
                // });
                const data = JSON.stringify(response);
                const parsed = JSON.parse(data);
                map(parsed, panel => {
                    const panelEvents = pathOr([], ['PanelEvents'], panel);
                    const communicators = pathOr([], ['Communicators'], panel);
                    const communicatorStatusUpdates = pathOr([], ['Communicators', 'CommunicatorStatusUpdates'], panel);
                    console.log(panel.Communicators.CommunicatorStatusUpdates)
                    // console.log(`Panel - ${panel.reference}`);
                    //
                    // map(panelEvents, event => {
                    //   // console.log(`- PanelEvents - ${panelEvents.length}`);
                    //   console.log(`- PanelEvents - ${event.reference}`);
                    //
                    //
                    // });
                    // map(communicators, communicator => {
                    //   // console.log(`- Communicators - ${communicators.length}`);
                    //   console.log(`- Communicators - ${communicator.reference}`);
                    //
                    //
                    // });
                    // map(communicatorStatusUpdates, csu => {
                    //     // console.log(`- - CommunicatorStatusUpdates - ${communicatorStatusUpdates.length}`);
                    //     console.log(`- - CommunicatorStatusUpdates - ${csu.reference}`);
                    //
                    // });
                });
            })
            .catch(error => {
                console.log('ERROR :: ', error);
            });
  }

}
