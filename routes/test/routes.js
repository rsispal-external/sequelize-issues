import TestFunctions from '../../functions/test/functions';

module.exports = [
  /*
  TEST FUNCTION 1: Request nested data with limits to demonstrate the error
  Route: localhost:8000/test/1

  Expected: Each `Panel` should have ONE `Communicator`. Each panel should have THREE `PanelEvents` and Each `Communicator` should have ONE `CommunicatorStatusUpdate`
  Actual: The data limit for `PanelEvents` and `CommunicatorStatusUpdates` has no effect and more than the limit are returned which is unacceptable as there could be 00's

  Visualisation of what SHOULD happen:
  Panel (ONE record)
  - Panel Events (THREE records)
  - Communicator (ONE record)
  - - CommunicatorStatusUpdates (ONE record)
  */

    {
        method: 'POST',
        path: `/test/1`,
        handler: async (request, reply) => {
            const res = await TestFunctions.testFunction1({ references: ['PANEL00001', 'PANEL00002', 'PANEL00003' ] })
            .catch(error => {
                return reply.response(error);
            });

            return reply.response(res);
        }
    }
];
