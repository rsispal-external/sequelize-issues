/*
*     Sequelize Test Bed
*
*     Instructions:
*     1. Create "test-database"
*     2. Ensure the username and password matches in ./database/connection/local-db/connection.js (default postgres postgres)
*     3. Set forcefullySyncDatabasesWithSeedData to true/false below to seed the database
*/
import Hapi from 'hapi';
import { delay } from 'lodash';

/* Database Model */
import LocalDBModel from './database/models/model.js';

/* General Database Operations */
import LocalDBOperations from './database/operations/local/operations'

/* Functions To Run In This Testbed */
import TestFunctions from './functions/test/functions';

/* General Database Operations */
import routes from './routes/';

/* Choose whether to pre-fill the database */
const forcefullySyncDatabasesWithSeedData = true;

switch (forcefullySyncDatabasesWithSeedData) {
    case true: {
        LocalDBOperations.syncAndSeedDatabases()
            .then(response => {
                if (response !== true) {
                    throw new Error('*** ERROR *** \nDatabase models could not be synced! Exiting...', response);
                }
            });
            break;
    }
    default: {
        LocalDBOperations.syncDatabases()
            .then(response => {
                if (response !== true) {
                    throw new Error('*** ERROR *** \nDatabase models could not be synced! Exiting...', response);
                }
            });
            break;
    }
}
/* Provide all permissiosn to "postgres" user account */
LocalDBOperations.setPermissions()
    .then(response => {
        if (response !== true) {
            throw new Error('*** ERROR *** \nDatabase permissions encountered an issue', response);
        }
    });



// Create a server with a host and port
const srvPort = process.env.PORT || 8000;
const srvAddr = '0.0.0.0';
const server = new Hapi.Server({
    host: srvAddr,
    port: srvPort,
    routes: {
        cors: {
            origin: ['*'],
            additionalHeaders: ['Accept', 'Authorization', 'Content-Type', 'If-None-Match', 'Accept-language']
        },
        validate: {
            options: {
                abortEarly: false
            }
        },
        timeout: {
            socket: 11 * 60 * 1000,
            server: false
        }
    }
});

server.route(routes);
server.start();

server.events.on('start', (route) => {
    console.log('Listening on port 8000... ');
});

server.events.on('log', (event, tags) => {
    if (tags.error) {
        console.log(`Server error: ${event.error ? event.error.message : 'unknown'}`);
    }
});
