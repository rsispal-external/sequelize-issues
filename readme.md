# Instructions

A basic HapiJS-based API with Sequelize to help demo problems encountered with Sequelize.

App runs on `port 8000` on `localhost`

## Prerequesites
1. Node 8+
2. Node Package Manager
3. A local PostgreDB installation

## PostgreDB Configuration
1. Ensure a database called `test-database` exists **prior** to running the application.
2. Make sure the username and password is `postgres`

## Installation instructions
1. `npm install`
2. Set the `forcefullySyncDatabasesWithSeedData` flag in `main.js` to seed the local test database.
3. `npm run start`
4. make a POST request on a specific route to run the test. Will be explained in a Git issue most likely.


File structure:
```
sequelize-testbed
- database
- - connection
- - - local-db
- - - - connection.js (this is where the sequelize connection is established)
- - models
- - - -[models]
- - - model.js (this is where all the models are synced and seeded)
- - operations
- - - operations that chain together functions in the model
- - schemas
- - - -[schemas]
- - seed
- - - [seed]
- - - - seed.js (the seed file corresponding to the model, ensure it's imported in `model.js`)
- functions
- - test
- - - functions.js
- - [other tests]
- routes
- - test
- - - routes.js
- - [other routes] (make sure these are imported in routes/index.js)
- - index.js
```
