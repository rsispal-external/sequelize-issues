import PanelEventsSchema from '../../schemas/panel-events/schema';

module.exports = sequelize => {
    const PanelEvents = sequelize.define('PanelEvents', PanelEventsSchema(), { underscored: true, freezeTableName: true, name: { single: 'PanelEvents', plural: 'PanelEvents' } });

    PanelEvents.associate = models => {
        PanelEvents.belongsTo(models.Panels, { foreignKey: 'reference', sourceKey: 'panelReference', constraints: false });
    };
    return PanelEvents;
};
