import Sequelize from 'sequelize';

/* Connection Wrappers */
import LocalDBConnection from '../connection/local-db/connection';

/* Seed Data */
import CommunicatorStatusUpdatesSeedData from '../seed/communicator-status-updates/seed';
import CommunicatorsSeedData from '../seed/communicators/seed';
import PanelEventsSeedData from '../seed/panel-events/seed';
import PanelsSeedData from '../seed/panels/seed';

const LocalDB = LocalDBConnection.getConnection();

const LocalDBModel = {
    async get() {
        const allModels = await this.prepareAllModels();
        const dbInstance = await this.importAllModels(allModels);
        return dbInstance;
    },

    async syncModelsAndGet() {
        const allModels = await this.prepareAllModels();
        const dbInstance = await this.importAllModels(allModels);
        await this.syncModels(dbInstance, allModels);
        return dbInstance;
    },

    async syncModelsSeedAndGet() {
        const allModels = await this.prepareAllModels();
        const dbInstance = await this.importAllModels(allModels);
        await this.syncAndSeedModels(dbInstance, allModels);
        return dbInstance;
    },

    async prepareAllModels() {
        const allModels = {
            CommunicatorStatusUpdates: LocalDB.import('./communicator-status-updates/model'),
            Communicators: LocalDB.import('./communicators/model'),
            PanelEvents: LocalDB.import('./panel-events/model'),
            Panels: LocalDB.import('./panels/model')
        };
        return allModels;
    },

    async importAllModels(allModels) {
        const dbInstance = allModels;
        Object.keys(allModels).forEach(modelName => {
            if ('associate' in allModels[modelName]) {
                dbInstance[modelName].associate(dbInstance);
            }
        });
        dbInstance.LocalDB = LocalDB;
        dbInstance.sequelize = LocalDB;
        dbInstance.Sequelize = Sequelize;
        return dbInstance;
    },

    async syncModels(dbInstance) {
        dbInstance.sequelize.sync({
                force: false
            }).then(async () => {
                console.log(`SYNC -> models synced!`);
            }).catch(error => {
                console.log(`-> models not synced due to an error! => `, error);
            });
    },

    async syncAndSeedModels(dbInstance) {
        dbInstance.sequelize.sync({
                force: true
            }).then(async () => {
                console.log(`SYNC -> models synced!`);
                await this.seedAllModels(dbInstance);
            }).catch(error => {
                console.log(`-> models not synced due to an error! => `, error);
            });
    },

    async seedAllModels(dbInstance) {
        const allModels = await this.prepareAllModels();

        const seedMap = {
            CommunicatorStatusUpdates: CommunicatorStatusUpdatesSeedData,
            Communicators: CommunicatorsSeedData,
            PanelEvents: PanelEventsSeedData,
            Panels: PanelsSeedData
        };
        Object.keys(allModels).forEach(modelName => {
            if (seedMap[modelName]) {
                dbInstance[modelName].bulkCreate(seedMap[modelName])
                    .then(() => {
                        console.log(`SEED -> Seeded ${modelName} with test data`);
                    }).catch(error => {
                        console.log(`-> ${modelName} model not seeded due to an error!`, error.message);
                    });
            }
        });
    },

    async setPermissions() {
        const allQueries = await this.generatePermissionQueries();
        LocalDB.query(allQueries)
            .then(() => {
                console.log('Permission Query Ran.');
                // console.log(`Permission Query Ran: ${curr}`);
            })
            .catch(err => {
                console.error('Unable to connect to the database:', err);
            });
    },

    async generatePermissionQueries() {
        const allModels = await this.prepareAllModels();
        const queryString = '';

        Object.keys(allModels).forEach(modelName => {
            const sequenceRoleAdmin = `GRANT ALL ON SEQUENCE public."${modelName}_id_seq" TO "postgres" WITH GRANT OPTION;`;
            const grantRoleAdmin = `GRANT ALL ON TABLE public."${modelName}" TO "postgres" WITH GRANT OPTION;`;
            queryString.concat(sequenceRoleAdmin);
            queryString.concat(grantRoleAdmin);
        });
        return queryString;
    }


};

export default LocalDBModel;
