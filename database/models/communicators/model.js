import CommunicatorsSchema from '../../schemas/communicators/schema';

module.exports = sequelize => {
    const Communicators = sequelize.define('Communicators', CommunicatorsSchema(), { underscored: true, freezeTableName: true, name: { single: 'Communicators', plural: 'Communicators'} });

    Communicators.associate = models => {
        Communicators.hasMany(models.CommunicatorStatusUpdates, { foreignKey: 'communicatorReference', sourceKey: 'reference', constraints: false });
    };

    return Communicators;
};
