import CommunicatorStatusUpdatesSchema from '../../schemas/communicator-status-updates/schema';

module.exports = sequelize => {
    const CommunicatorStatusUpdates = sequelize.define('CommunicatorStatusUpdates', CommunicatorStatusUpdatesSchema(), { underscored: true, freezeTableName: true, name: { single: 'CommunicatorStatusUpdates', plural: 'CommunicatorStatusUpdates' } });

    CommunicatorStatusUpdates.associate = models => {
        CommunicatorStatusUpdates.belongsTo(models.Communicators, { foreignKey: 'reference', sourceKey: 'communicatorReference', constraints: false });
    };

    return CommunicatorStatusUpdates;
};
