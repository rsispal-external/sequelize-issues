import PanelsSchema from '../../schemas/panels/schema';

module.exports = sequelize => {
    const Panels = sequelize.define('Panels', PanelsSchema(), { underscored: true, freezeTableName: true, name: { single: 'Panels', plural: 'Panels' } });

    Panels.associate = models => {
        Panels.hasMany(models.Communicators, { foreignKey: 'panelReference', sourceKey: 'reference', constraints: false });
        Panels.hasMany(models.CommunicatorStatusUpdates, { foreignKey: 'panelReference', sourceKey: 'reference', constraints: false });
        Panels.hasMany(models.PanelEvents, { foreignKey: 'panelReference', sourceKey: 'reference', constraints: false });
    };
    return Panels;
};
