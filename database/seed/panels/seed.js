export default [
  {
    "status" : "ACTIVE",
    "id" : 1,
    "reference" : "PANEL00001",
    "model" : "PANEL-X",
    "updated_at" : "2018-07-11 01:00:01+01",
    "created_at" : "2018-07-11 01:00:01+01"
  },
  {
    "status" : "ACTIVE",
    "id" : 2,
    "reference" : "PANEL00002",
    "model" : "PANEL-Y",
    "updated_at" : "2018-07-11 01:00:01+01",
    "created_at" : "2018-07-11 01:00:01+01"
  },
  {
    "status" : "ACTIVE",
    "id" : 3,
    "reference" : "PANEL00003",
    "model" : "PANEL-Z",
    "updated_at" : "2018-07-11 01:00:01+01",
    "created_at" : "2018-07-11 01:00:01+01"
  }
]
