export default [
  {
    "created_at" : "2018-07-11 01:00:01+01",
    "id" : 1,
    "reference" : "COMMUNICATO0001",
    "model" : "MODEL-A",
    "updated_at" : "2018-07-11 01:00:01+01",
    "panelReference" : "PANEL00001"
  },
  {
    "created_at" : "2018-07-11 01:00:01+01",
    "id" : 2,
    "reference" : "COMMUNICATO0002",
    "model" : "MODEL-B",
    "updated_at" : "2018-07-11 01:00:01+01",
    "panelReference" : "PANEL00002"
  },
  {
    "created_at" : "2018-07-11 01:00:01+01",
    "id" : 3,
    "reference" : "COMMUNICATO0003",
    "model" : "MODEL-C",
    "updated_at" : "2018-07-11 01:00:01+01",
    "panelReference" : "PANEL00003"
  }
]
