export default [
  {
    "reference" : "CS0000000001",
    "status" : "OK",
    "id" : 1,
    "created_at" : "2018-07-11 01:00:01+01",
    "panelReference" : "PANEL00001",
    "communicatorReference" : "COMMUNICATO0001",
    "updated_at" : "2018-07-11 01:00:01+01",
    "timestamp" : "2018-07-11 15:55:00+01"
  },
  {
    "reference" : "CS0000000002",
    "status" : "OK",
    "id" : 2,
    "created_at" : "2018-07-11 01:00:01+01",
    "panelReference" : "PANEL00001",
    "communicatorReference" : "COMMUNICATO0001",
    "updated_at" : "2018-07-11 01:00:01+01",
    "timestamp" : "2018-07-11 15:25:00+01"
  },
  {
    "reference" : "CS0000000003",
    "status" : "OK",
    "id" : 3,
    "created_at" : "2018-07-11 01:00:01+01",
    "panelReference" : "PANEL00001",
    "communicatorReference" : "COMMUNICATO0001",
    "updated_at" : "2018-07-11 01:00:01+01",
    "timestamp" : "2018-07-11 14:55:00+01"
  },
  {
    "reference" : "CS0000000004",
    "status" : "OK",
    "id" : 4,
    "created_at" : "2018-07-11 01:00:01+01",
    "panelReference" : "PANEL00001",
    "communicatorReference" : "COMMUNICATO0001",
    "updated_at" : "2018-07-11 01:00:01+01",
    "timestamp" : "2018-07-11 14:25:00+01"
  },
  {
    "reference" : "CS0000000005",
    "status" : "OK",
    "id" : 5,
    "created_at" : "2018-07-11 01:00:01+01",
    "panelReference" : "PANEL00002",
    "communicatorReference" : "COMMUNICATO0002",
    "updated_at" : "2018-07-11 01:00:01+01",
    "timestamp" : "2018-07-11 15:55:00+01"
  },
  {
    "reference" : "CS0000000006",
    "status" : "LOW-BATTERY",
    "id" : 6,
    "created_at" : "2018-07-11 01:00:01+01",
    "panelReference" : "PANEL00002",
    "communicatorReference" : "COMMUNICATO0002",
    "updated_at" : "2018-07-11 01:00:01+01",
    "timestamp" : "2018-07-11 15:25:00+01"
  },
  {
    "reference" : "CS0000000007",
    "status" : "OK",
    "id" : 7,
    "created_at" : "2018-07-11 01:00:01+01",
    "panelReference" : "PANEL00002",
    "communicatorReference" : "COMMUNICATO0002",
    "updated_at" : "2018-07-11 01:00:01+01",
    "timestamp" : "2018-07-11 14:55:00+01"
  },
  {
    "reference" : "CS0000000008",
    "status" : "WIFI-RESTORE",
    "id" : 8,
    "created_at" : "2018-07-11 01:00:01+01",
    "panelReference" : "PANEL00002",
    "communicatorReference" : "COMMUNICATO0002",
    "updated_at" : "2018-07-11 01:00:01+01",
    "timestamp" : "2018-07-11 14:25:00+01"
  },
  {
    "reference" : "CS0000000009",
    "status" : "OK",
    "id" : 9,
    "created_at" : "2018-07-11 01:00:01+01",
    "panelReference" : "PANEL00003",
    "communicatorReference" : "COMMUNICATO0003",
    "updated_at" : "2018-07-11 01:00:01+01",
    "timestamp" : "2018-07-11 15:55:00+01"
  },
  {
    "reference" : "CS0000000010",
    "status" : "OK",
    "id" : 10,
    "created_at" : "2018-07-11 01:00:01+01",
    "panelReference" : "PANEL00003",
    "communicatorReference" : "COMMUNICATO0003",
    "updated_at" : "2018-07-11 01:00:01+01",
    "timestamp" : "2018-07-11 15:25:00+01"
  },
  {
    "reference" : "CS0000000011",
    "status" : "OK",
    "id" : 11,
    "created_at" : "2018-07-11 01:00:01+01",
    "panelReference" : "PANEL00003",
    "communicatorReference" : "COMMUNICATO0003",
    "updated_at" : "2018-07-11 01:00:01+01",
    "timestamp" : "2018-07-11 14:55:00+01"
  },
  {
    "reference" : "CS0000000012",
    "status" : "FIRMWARE-UPDATED",
    "id" : 12,
    "created_at" : "2018-07-11 01:00:01+01",
    "panelReference" : "PANEL00003",
    "communicatorReference" : "COMMUNICATO0003",
    "updated_at" : "2018-07-11 01:00:01+01",
    "timestamp" : "2018-07-11 14:25:00+01"
  }
]
