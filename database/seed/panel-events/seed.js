export default [
  {
    "reference" : "PE0000000001",
    "id" : 1,
    "eventType" : "FIRE",
    "panelReference" : "PANEL00001",
    "created_at" : "2018-07-11 01:00:01+01",
    "updated_at" : "2018-07-11 01:00:01+01",
    "timestamp" : "2018-07-11 16:08:00+01"
  },
  {
    "reference" : "PE0000000002",
    "id" : 2,
    "eventType" : "FAULT",
    "panelReference" : "PANEL00001",
    "created_at" : "2018-07-11 01:00:01+01",
    "updated_at" : "2018-07-11 01:00:01+01",
    "timestamp" : "2018-07-11 16:07:00+01"
  },
  {
    "reference" : "PE0000000003",
    "id" : 3,
    "eventType" : "FIRE",
    "panelReference" : "PANEL00001",
    "created_at" : "2018-07-11 01:00:01+01",
    "updated_at" : "2018-07-11 01:00:01+01",
    "timestamp" : "2018-07-11 16:06:00+01"
  },
  {
    "reference" : "PE0000000004",
    "id" : 4,
    "eventType" : "RESET",
    "panelReference" : "PANEL00001",
    "created_at" : "2018-07-11 01:00:01+01",
    "updated_at" : "2018-07-11 01:00:01+01",
    "timestamp" : "2018-07-11 16:05:00+01"
  },
  {
    "reference" : "PE0000000005",
    "id" : 5,
    "eventType" : "POLL",
    "panelReference" : "PANEL00001",
    "created_at" : "2018-07-11 01:00:01+01",
    "updated_at" : "2018-07-11 01:00:01+01",
    "timestamp" : "2018-07-11 16:04:00+01"
  },
  {
    "reference" : "PE0000000006",
    "id" : 6,
    "eventType" : "STATUS",
    "panelReference" : "PANEL00001",
    "created_at" : "2018-07-11 01:00:01+01",
    "updated_at" : "2018-07-11 01:00:01+01",
    "timestamp" : "2018-07-11 16:03:00+01"
  },
  {
    "reference" : "PE0000000007",
    "id" : 7,
    "eventType" : "DISABLEMENT",
    "panelReference" : "PANEL00001",
    "created_at" : "2018-07-11 01:00:01+01",
    "updated_at" : "2018-07-11 01:00:01+01",
    "timestamp" : "2018-07-11 16:02:00+01"
  },
  {
    "reference" : "PE0000000008",
    "id" : 8,
    "eventType" : "EVENT#X",
    "panelReference" : "PANEL00001",
    "created_at" : "2018-07-11 01:00:01+01",
    "updated_at" : "2018-07-11 01:00:01+01",
    "timestamp" : "2018-07-11 16:01:00+01"
  },
  {
    "reference" : "PE0000000009",
    "id" : 9,
    "eventType" : "FIRE",
    "panelReference" : "PANEL00002",
    "created_at" : "2018-07-11 01:00:01+01",
    "updated_at" : "2018-07-11 01:00:01+01",
    "timestamp" : "2018-07-11 16:08:00+01"
  },
  {
    "reference" : "PE0000000010",
    "id" : 10,
    "eventType" : "FAULT",
    "panelReference" : "PANEL00002",
    "created_at" : "2018-07-11 01:00:01+01",
    "updated_at" : "2018-07-11 01:00:01+01",
    "timestamp" : "2018-07-11 16:07:00+01"
  },
  {
    "reference" : "PE0000000011",
    "id" : 11,
    "eventType" : "FIRE",
    "panelReference" : "PANEL00002",
    "created_at" : "2018-07-11 01:00:01+01",
    "updated_at" : "2018-07-11 01:00:01+01",
    "timestamp" : "2018-07-11 16:06:00+01"
  },
  {
    "reference" : "PE0000000012",
    "id" : 12,
    "eventType" : "RESET",
    "panelReference" : "PANEL00002",
    "created_at" : "2018-07-11 01:00:01+01",
    "updated_at" : "2018-07-11 01:00:01+01",
    "timestamp" : "2018-07-11 16:05:00+01"
  },
  {
    "reference" : "PE0000000013",
    "id" : 13,
    "eventType" : "POLL",
    "panelReference" : "PANEL00002",
    "created_at" : "2018-07-11 01:00:01+01",
    "updated_at" : "2018-07-11 01:00:01+01",
    "timestamp" : "2018-07-11 16:04:00+01"
  },
  {
    "reference" : "PE0000000014",
    "id" : 14,
    "eventType" : "STATUS",
    "panelReference" : "PANEL00002",
    "created_at" : "2018-07-11 01:00:01+01",
    "updated_at" : "2018-07-11 01:00:01+01",
    "timestamp" : "2018-07-11 16:03:00+01"
  },
  {
    "reference" : "PE0000000015",
    "id" : 15,
    "eventType" : "DISABLEMENT",
    "panelReference" : "PANEL00002",
    "created_at" : "2018-07-11 01:00:01+01",
    "updated_at" : "2018-07-11 01:00:01+01",
    "timestamp" : "2018-07-11 16:02:00+01"
  },
  {
    "reference" : "PE0000000016",
    "id" : 16,
    "eventType" : "EVENT#X",
    "panelReference" : "PANEL00002",
    "created_at" : "2018-07-11 01:00:01+01",
    "updated_at" : "2018-07-11 01:00:01+01",
    "timestamp" : "2018-07-11 16:01:00+01"
  },
  {
    "reference" : "PE0000000017",
    "id" : 17,
    "eventType" : "FIRE",
    "panelReference" : "PANEL00003",
    "created_at" : "2018-07-11 01:00:01+01",
    "updated_at" : "2018-07-11 01:00:01+01",
    "timestamp" : "2018-07-11 16:08:00+01"
  },
  {
    "reference" : "PE0000000018",
    "id" : 18,
    "eventType" : "FAULT",
    "panelReference" : "PANEL00003",
    "created_at" : "2018-07-11 01:00:01+01",
    "updated_at" : "2018-07-11 01:00:01+01",
    "timestamp" : "2018-07-11 16:07:00+01"
  },
  {
    "reference" : "PE0000000019",
    "id" : 19,
    "eventType" : "FIRE",
    "panelReference" : "PANEL00003",
    "created_at" : "2018-07-11 01:00:01+01",
    "updated_at" : "2018-07-11 01:00:01+01",
    "timestamp" : "2018-07-11 16:06:00+01"
  },
  {
    "reference" : "PE0000000020",
    "id" : 20,
    "eventType" : "RESET",
    "panelReference" : "PANEL00003",
    "created_at" : "2018-07-11 01:00:01+01",
    "updated_at" : "2018-07-11 01:00:01+01",
    "timestamp" : "2018-07-11 16:05:00+01"
  },
  {
    "reference" : "PE0000000021",
    "id" : 21,
    "eventType" : "POLL",
    "panelReference" : "PANEL00003",
    "created_at" : "2018-07-11 01:00:01+01",
    "updated_at" : "2018-07-11 01:00:01+01",
    "timestamp" : "2018-07-11 16:04:00+01"
  },
  {
    "reference" : "PE0000000022",
    "id" : 22,
    "eventType" : "STATUS",
    "panelReference" : "PANEL00003",
    "created_at" : "2018-07-11 01:00:01+01",
    "updated_at" : "2018-07-11 01:00:01+01",
    "timestamp" : "2018-07-11 16:03:00+01"
  },
  {
    "reference" : "PE0000000023",
    "id" : 23,
    "eventType" : "DISABLEMENT",
    "panelReference" : "PANEL00003",
    "created_at" : "2018-07-11 01:00:01+01",
    "updated_at" : "2018-07-11 01:00:01+01",
    "timestamp" : "2018-07-11 16:02:00+01"
  },
  {
    "reference" : "PE0000000024",
    "id" : 24,
    "eventType" : "EVENT#X",
    "panelReference" : "PANEL00003",
    "created_at" : "2018-07-11 01:00:01+01",
    "updated_at" : "2018-07-11 01:00:01+01",
    "timestamp" : "2018-07-11 16:01:00+01"
  }
]
