/* Database Model */
import LocalDBModel from '../../models/model';

const LocalDBOperations = {
    async syncDatabases(preferences) {
        const Model = await LocalDBModel.syncModelsAndGet(preferences);
        return Model.LocalDB
            .authenticate()
            .then(() => {
                console.log('Connection to database has been established successfully.');
                return true;
            })
            .catch(err => {
                console.error('Unable to connect to the database:', err);
                return err;
            });
    },
    async syncAndSeedDatabases(preferences) {
        const Model = await LocalDBModel.syncModelsSeedAndGet(preferences);
        return Model.LocalDB
            .authenticate()
            .then(() => {
                console.log('Connection to database has been established successfully.');
                return true;
            })
            .catch(err => {
                console.error('Unable to connect to the database:', err);
                return err;
            });
    },
    async setPermissions() {
        await LocalDBModel.setPermissions();
        return true;
    }

};

export default LocalDBOperations;
