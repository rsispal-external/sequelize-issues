import Sequelize from 'sequelize';

const DataTypes = Sequelize.DataTypes;

module.exports = () => {
    return {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
            allowNull: false
        },
        reference: {
            type: DataTypes.STRING(512),
            unique: true,
            allowNull: false
        },
        panelReference: {
            type: DataTypes.STRING(100),
            allowNull: false
        },
        eventType: {
            type: DataTypes.STRING(32),
            allowNull: false
        },
        timestamp: {
            type: DataTypes.DATE
        }
    };
};
