import Sequelize from 'sequelize';

const DataTypes = Sequelize.DataTypes;

module.exports = () => {
    return {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
            allowNull: false
        },
        reference: {
            type: DataTypes.STRING(100),
            unique: true,
            allowNull: false
        },
        communicatorReference: {
            type: DataTypes.STRING(100),
            allowNull: false
        },
        panelReference: {
            type: DataTypes.STRING(100),
            allowNull: false
        },
        timestamp: {
            type: DataTypes.DATE
        },
        status: {
            type: DataTypes.STRING(20),
            allowNull: false
        }
    };
};


// This connection verification is separate to the access tokens. It's in addition to this, and will determine whether the app needs to alert the user to a connection issue. If ANY panel has no verification in the past 60 minutes then it needs to be investigated
