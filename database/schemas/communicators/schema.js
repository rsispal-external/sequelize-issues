import Sequelize from 'sequelize';

const DataTypes = Sequelize.DataTypes;

module.exports = () => {
    return {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
            allowNull: false
        },
        reference: {
            type: DataTypes.STRING(100),
            unique: true,
            allowNull: false
        },
        model: {
            type: DataTypes.STRING(100),
            allowNull: false
        },
        panelReference: {
            type: DataTypes.STRING(100),
            unique: false,
            allowNull: false
        }
    };
};
